# README #

Relevant information about the project:

### What is this repository for? ###

This repository is the solution to the technical challenge by Sooft Technology.

To take into consideration:

-Each API developed has its respective documentation in the Javadoc.

-The following are the example Request Bodies to consume the PUT and POST APIs of the entities related to a career:

    -For the entity "Licenciatura"
  {
	"nombre":"Ciencias de la Computacion 2",
	"facultad" : "Ciencias",
	"duracion" : 5,
	"tituloIntermedio" : true,
	"modalidad" : "Anual",
	"requiereTesis" : true
}
    -For the entity "Ingenieria"
{
	"nombre":"Ingenieria Sistema",
	"facultad" : "Ciencias",
	"duracion" : 5,
	"tituloIntermedio" : true,
	"modalidad" : "Anual"
}


### How do I get set up? ###

All the dependencies needed to run the app are included on the POM file. To run the application, just run the Application.java file as 'Java Application'  which is located on the 'ar.edu.teclab.prueba' package.

### Who do I talk to? ###

* Repo owner or admin