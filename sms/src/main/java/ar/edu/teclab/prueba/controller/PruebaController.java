package ar.edu.teclab.prueba.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import ar.edu.teclab.prueba.dto.Comment;
import ar.edu.teclab.prueba.dto.Ejemplo;
import ar.edu.teclab.prueba.dto.Ticket;
import ar.edu.teclab.prueba.entity.Ingenieria;
import ar.edu.teclab.prueba.entity.Licenciatura;
import ar.edu.teclab.prueba.repository.DaoIngenieria;
import ar.edu.teclab.prueba.repository.DaoLicenciatura;

@RestController
@RequestMapping("/test")
@CrossOrigin(origins = "*")
public class PruebaController {

	private static final Log LOG = LogFactory.getLog(PruebaController.class);
	
	@Autowired
	private DaoLicenciatura daoLicenciatura;

	@Autowired
	private DaoIngenieria daoIngenieria;
	
	@GetMapping("/ejemplo")
	public ResponseEntity<Ejemplo> getMessageStatus(@RequestParam(value ="nombre") String nombre) {
		try {
			Ejemplo ejemplo = new Ejemplo();
//			ejemplo.setNombre(nombre);
			return ResponseEntity.ok(ejemplo);
		}catch (Exception e){
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * GET API which returns the list of comments of a ticket passed as a parameter from the services of Techlab.
	 * @param id passed as a path variable which is the id of the ticket to consult.
	 * @return A json object with the comments of a ticket passed as a parameter. **/
	@GetMapping("/getCommentsFromTicket/{id}")
	public ResponseEntity<Object> getCommentById(@PathVariable("id") String id) {
		try {
				
				// API GET uri to call.
			   final String uri = "https://teclab1593636133.zendesk.com/api/v2/tickets/{id}/comments";
			   RestTemplate restTemplate = new RestTemplate();		   
			   Map<String, String> params = new HashMap<String, String>();
			   params.put("id", id);
			   
			   //Credential setting to consume Techlab's external API.
			   String plainCreds = "jorge.danni@teclab.edu.ar:Abril2019";
			   byte[] plainCredsBytes = plainCreds.getBytes();
			   byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			   String base64Creds = new String(base64CredsBytes);
			   
			   //Parameters setting in the authorization header to consume the GET API from Techlab.
			   HttpHeaders headers = new HttpHeaders();
			   headers.add("Authorization", "Basic " + base64Creds);
			   HttpEntity<String> request = new HttpEntity<String>(headers);
			   
			   //Method that executes the HTTP request.
			   ResponseEntity<Object> response = restTemplate.exchange(uri, HttpMethod.GET, request, Object.class, params);
			
			return ResponseEntity.ok().body(response.getBody());
		}catch (Exception e){
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * PUT API which inserts a new comment on a ticket passed as a parameter from the services of Techlab
	 * @param id passed as a path variable that represents the id of the ticket on which the api inserts the new comment.
	 * @param ejemplo represents a ticket object in Json format passed as a body parameter. For more information, consult the Techlab's api documentation.   
	 * @return A Json object with the new comment of the ticket passed as a parameter. **/
	@PutMapping("/postCommentOnTicket/{id}")
	public ResponseEntity<Object> postCommentOnTicket(@PathVariable("id") String id, @RequestBody Ejemplo ejemplo) {
		try {
			
			//Construction of a new comment object.
			Ticket newTicket = new Ticket();
			Ejemplo newEjemplo = new Ejemplo();
			Comment newComment = new Comment();
			newComment.setBody(ejemplo.getTicket().getComment().getBody());
			newComment.setPublic(ejemplo.getTicket().getComment().getPublic());
			newTicket.setComment(newComment);
			newTicket.setStatus(ejemplo.getTicket().getStatus());
			newEjemplo.setTicket(newTicket);
			
			// API PUT uri to call.
			final String uri = "https://teclab1593636133.zendesk.com/api/v2/tickets/{id}";
			RestTemplate restTemplate = new RestTemplate();
			Map<String, String> params = new HashMap<String, String>();
			params.put("id", id);
			
			//Credential setting to consume Techlab's external API.
			String plainCreds = "jorge.danni@teclab.edu.ar:Abril2019";
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
			
			//Parameters setting in the authorization header to consume the PUT API from Techlab.
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", "Basic " + base64Creds);
			//new object setting in the request params to consume the PUT API from Techlab.
			HttpEntity<Ejemplo> request = new HttpEntity<Ejemplo>(newEjemplo, headers);
			
			 //Method that executes the HTTP request.
			ResponseEntity<Object> response = restTemplate.exchange(uri, HttpMethod.PUT, request, Object.class, params);
		
		return ResponseEntity.ok().body(response.getBody());
		
		}
		catch(Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * GET API which returns all the 'Licenciatura' objects of the database.  
	 * @return A Json array object with all the 'Licenciatura' objects existing on the database. **/
	@GetMapping(path= {"/listarLicenciaturas"})
	public ResponseEntity<Object> getLicenciaturas(){
		try {
		
		List<Licenciatura> licenciaturasEncontradas = daoLicenciatura.findAll();
		JsonArray json_array = new JsonArray();
		
		if (licenciaturasEncontradas != null) {
		
			for (Licenciatura lic: licenciaturasEncontradas) {
				JsonObject aux = new JsonObject();
				
				aux.addProperty("Id: ", lic.getId());
				aux.addProperty("Nombre: ", lic.getNombre());
				aux.addProperty("Facultad: ", lic.getFacultad());
				aux.addProperty("Duracion: ", lic.getDuracion());
				aux.addProperty("Titulo Intermedio: ", lic.getTituloIntermedio());
				aux.addProperty("Modalidad: ", lic.getModalidad());
				aux.addProperty("Requiere Tesis: ", lic.getRequiereTesis());
					json_array.add(aux);;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",0);
			obj.add("Results: ", json_array);
			return ResponseEntity.ok().body(obj);
		} else {
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",1);
			obj.addProperty("Mensaje: ", "Could not find any registry.");
		}

		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * POST API which inserts a new 'Licenciatura' registry on the database.
	 * @param licenciatura represents a 'Licenciatura' object in Json format passed as a body parameter.   
	 * @return The new Json object to persist. **/
	@PostMapping (path= {"/crearLicenciatura"})
	public ResponseEntity<Licenciatura> crearLicenciatura(@RequestBody Licenciatura licenciatura) {
		try {
		daoLicenciatura.save(licenciatura);
		
		JsonObject obj = new JsonObject();
		obj.addProperty("Error: ",0);
		obj.addProperty("Mensaje: ",licenciatura.toString());
		
		return ResponseEntity.ok().body(licenciatura); 
		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * PUT API which modifies an existing 'Licenciatura' registry on the database.
	 * @param id passed as a path variable that represents the id of the 'Licenciatura' object which the api modifies.
	 * @param licenciatura represents a 'Licenciatura' object in Json format passed as a body parameter with the new values to modify.   
	 * @return The modified object. **/
	@PutMapping (path= {"/modificarLicenciatura/{id}"})
	public ResponseEntity<Licenciatura> modificarLicenciatura(@PathVariable("id") Long id,
			@RequestBody Licenciatura licenciatura) {
		try {
		Licenciatura licMod = daoLicenciatura.findOne(id);
		
		if (licMod != null) {
		licMod.setNombre(licenciatura.getNombre());
		licMod.setFacultad(licenciatura.getFacultad());
		licMod.setDuracion(licenciatura.getDuracion());
		licMod.setTituloIntermedio(licenciatura.getTituloIntermedio());
		licMod.setModalidad(licenciatura.getModalidad());
		licMod.setRequiereTesis(licenciatura.getRequiereTesis());
		
		final Licenciatura updLic = daoLicenciatura.save(licMod);
		
		
		JsonObject obj = new JsonObject();
		obj.addProperty("Error: ",0);
		obj.addProperty("Mensaje: ",licenciatura.toString());
		
		return ResponseEntity.ok().body(updLic);
		}
		else {
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",1);
			obj.addProperty("Mensaje: ", "Licenciatura not found for this id " + id);
		}
		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * DELETE API which deletes an existing 'Licenciatura' registry on the database.
	 * @param id passed as a path variable that represents the id of the 'Licenciatura' object to eliminate.
	 * @return The respective message of the transaction's result. **/
	@DeleteMapping (path= {"/eliminarLicenciatura/{id}"})
	public ResponseEntity<Object> eliminarLicenciatura(@PathVariable("id") Long id) {
		try {
		Licenciatura licMod = daoLicenciatura.findOne(id);
		
		if (licMod != null) {
		daoLicenciatura.delete(licMod);
		
		JsonObject obj = new JsonObject();
		obj.addProperty("Error: ",0);
		obj.addProperty("Mensaje: ", "Licenciatura with id " + id + " " + "deleted successfully.");
		
		return ResponseEntity.ok().body(obj);
		}
		else {
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",1);
			obj.addProperty("Mensaje: ", "Licenciatura not found for this id " + id);
		}
		
		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * GET API which returns all the 'Ingenieria' objects of the database.  
	 * @return A Json array object with all the 'Ingenieria' objects existing on the database. **/
	@GetMapping(path= {"/listarIngenierias"})
	public ResponseEntity<Object> getIngenierias(){
		try {
		
		List<Ingenieria> ingenieriasEncontradas = daoIngenieria.findAll();
		JsonArray json_array = new JsonArray();
		
		if (ingenieriasEncontradas != null) {
		
			for (Ingenieria ing: ingenieriasEncontradas) {
				JsonObject aux = new JsonObject();
				
				aux.addProperty("Id: ", ing.getId());
				aux.addProperty("Nombre: ", ing.getNombre());
				aux.addProperty("Facultad: ", ing.getFacultad());
				aux.addProperty("Duracion: ", ing.getDuracion());
				aux.addProperty("Titulo Intermedio: ", ing.getTituloIntermedio());
				aux.addProperty("Modalidad: ", ing.getModalidad());
					json_array.add(aux);;
			}
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",0);
			obj.add("Results: ", json_array);
			return ResponseEntity.ok().body(obj);
		} else {
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",1);
			obj.addProperty("Mensaje: ", "Could not find any registry.");
		}

		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * POST API which inserts a new 'Ingenieria' Object on the database.
	 * @param ingenieria represents a 'Ingenieria' object in Json format passed as a body parameter.   
	 * @return The new Json object to persist. **/
	@PostMapping (path= {"/crearIngenieria"})
	public ResponseEntity<Ingenieria> crearIngenieria(@RequestBody Ingenieria ingenieria) {
		try {
		daoIngenieria.save(ingenieria);
		
		JsonObject obj = new JsonObject();
		obj.addProperty("Error: ",0);
		obj.addProperty("Mensaje: ",ingenieria.toString());
		
		return ResponseEntity.ok().body(ingenieria); 
		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * PUT API which modifies an existing 'Ingenieria' registry on the database.
	 * @param id passed as a path variable that represents the id of the 'Ingenieria' object which the api modifies.
	 * @param represents a 'Ingenieria' object in Json format passed as a body parameter with the new values to modify.   
	 * @return The modified object. **/
	@PutMapping (path= {"/modificarIngenieria/{id}"})
	public ResponseEntity<Ingenieria> modificarIngenieria(@PathVariable("id") Long id,
			@RequestBody Ingenieria ingenieria) {
		try {
		Ingenieria ingMod = daoIngenieria.findOne(id);
		
		if (ingMod != null) {
		ingMod.setNombre(ingenieria.getNombre());
		ingMod.setFacultad(ingenieria.getFacultad());
		ingMod.setDuracion(ingenieria.getDuracion());
		ingMod.setTituloIntermedio(ingenieria.getTituloIntermedio());
		ingMod.setModalidad(ingenieria.getModalidad());
		
		final Ingenieria updIng = daoIngenieria.save(ingMod);
		
		
		JsonObject obj = new JsonObject();
		obj.addProperty("Error: ",0);
		obj.addProperty("Mensaje: ",ingenieria.toString());
		
		return ResponseEntity.ok().body(updIng);
		}
		else {
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",1);
			obj.addProperty("Mensaje: ", "Licenciatura not found for this id " + id);
		}
		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
	/**
	 * @author AndresValderrama
	 * DELETE API which deletes an existing 'Ingenieria' registry on the database.
	 * @param id passed as a path variable that represents the id of the 'Ingenieria' object to eliminate.
	 * @return The respective message of the transaction's result. **/
	@DeleteMapping (path= {"/eliminarIngenieria/{id}"})
	public ResponseEntity<Object> eliminarIngenieria(@PathVariable("id") Long id) {
		try {
		Ingenieria ingDel = daoIngenieria.findOne(id);
		
		if (ingDel != null) {
		daoIngenieria.delete(ingDel);
		
		JsonObject obj = new JsonObject();
		obj.addProperty("Error: ",0);
		obj.addProperty("Mensaje: ", "Ingenieria with id " + id + " " + "deleted successfully.");
		
		return ResponseEntity.ok().body(obj);
		}
		else {
			JsonObject obj = new JsonObject();
			obj.addProperty("Error: ",1);
			obj.addProperty("Mensaje: ", "Ingenieria not found for this id " + id);
		}
		
		}
		catch (Exception e) {
			LOG.error("Error", e);
		}
		return null;
	}
	
}