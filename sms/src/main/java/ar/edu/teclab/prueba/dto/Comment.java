package ar.edu.teclab.prueba.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

	@SerializedName("body")
	@Expose
	private String body;
	@SerializedName("public")
	@Expose
	private Boolean _public;

	public Comment() {}
	
	public String getBody() {
	return body;
	}

	public void setBody(String body) {
	this.body = body;
	}

	public Boolean getPublic() {
	return _public;
	}

	public void setPublic(Boolean _public) {
	this._public = _public;
	}
	
}
