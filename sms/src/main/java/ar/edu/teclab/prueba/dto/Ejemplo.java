package ar.edu.teclab.prueba.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ejemplo {
	
	@SerializedName("ticket")
	@Expose
	private Ticket ticket;
	
	public Ejemplo() {}

	public Ticket getTicket() {
	return ticket;
	}

	public void setTicket(Ticket ticket) {
	this.ticket = ticket;
	}
	
}
