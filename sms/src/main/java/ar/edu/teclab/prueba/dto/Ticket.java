package ar.edu.teclab.prueba.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ticket {

	@SerializedName("comment")
	@Expose
	private Comment comment;
	@SerializedName("status")
	@Expose
	private String status;
	
	public Ticket() {}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
