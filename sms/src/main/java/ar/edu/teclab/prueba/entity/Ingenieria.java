package ar.edu.teclab.prueba.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
@Table(name="ingenieria")
public class Ingenieria  {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@SerializedName("nombre")
	@Expose
	private String nombre;
	
	@SerializedName("facultad")
	@Expose
	private String facultad;
	
	@SerializedName("duracion")
	@Expose
	private int duracion;
	
	@SerializedName("tituloIntermedio")
	@Expose
	private Boolean tituloIntermedio;
	
	@SerializedName("modalidad")
	@Expose
	private String modalidad;
	
	public Ingenieria() {
		super();
	}
	
	public Ingenieria(String nombre, String facultad, int duracion, Boolean tituloIntermedio, Boolean requiereTesis, String planEstudio) {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFacultad() {
		return facultad;
	}

	public void setFacultad(String facultad) {
		this.facultad = facultad;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public Boolean getTituloIntermedio() {
		return tituloIntermedio;
	}

	public void setTituloIntermedio(Boolean tituloIntermedio) {
		this.tituloIntermedio = tituloIntermedio;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}
	
}
