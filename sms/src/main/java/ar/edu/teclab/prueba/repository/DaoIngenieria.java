package ar.edu.teclab.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.edu.teclab.prueba.entity.Ingenieria;

@Repository
public interface DaoIngenieria extends JpaRepository<Ingenieria, Long> {

}
