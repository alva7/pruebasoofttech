package ar.edu.teclab.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.edu.teclab.prueba.entity.Licenciatura;

@Repository
public interface DaoLicenciatura extends JpaRepository<Licenciatura, Long> {

	Licenciatura findOne(Long id);
}
